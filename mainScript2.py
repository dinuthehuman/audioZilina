#Bibliotheken
##############
import sounddevice as sd
from scipy.io.wavfile import write
import os.path
from pydub import AudioSegment
import simpleaudio as sa
import RPi.GPIO as GPIO
from time import sleep
import time
import datetime
from flask import Flask
import threading

#Konfiguration
###############
defaultSamplerate = 48000
duration = 300  #in Sekunden
motorPin = 21
channels = 1

runningMode = "OFF"

#Flask App
##########

app = Flask(__name__)

# Routes
@app.route('/')
def root():
    return app.send_static_file('index.html')


@app.route('/<path:path>')
def static_proxy(path):
    # send_static_file will guess the correct MIME type
    return app.send_static_file(path)

@app.route('/api/<cmd>')
def apiFunction(cmd):
    print(cmd)
    global runningMode
    runningMode = cmd
    return cmd


def runFlask():
    app.run(host='0.0.0.0')

flaskThread = threading.Thread(target=runFlask)


def recordToFile(strName):
    #print("Starting recording...")
    myrecording = sd.rec(int(defaultSamplerate*duration), samplerate=defaultSamplerate, channels=1)
    sd.wait()
    #print("Timer reached, recording done")
    write(strName, defaultSamplerate, myrecording)
    sound1 = AudioSegment.from_file(strName)
    sound1.export(strName, format='wav')


def combineFiles(oldFile, newFile):
    #print("Combining Files...")
    sound1 = AudioSegment.from_file(oldFile)
    sound2 = AudioSegment.from_file(newFile)
    combined = sound1.overlay(sound2)
    combined.export(oldFile, format='wav')
    print("...done")


def playFile(fileToPlay):
    #print("Playback...")
    wave_obj = sa.WaveObject.from_wave_file(fileToPlay)
    play_obj = wave_obj.play()
    play_obj.wait_done()
    #print("...done")


def checkTime():
    dayArray = [1, 2, 3, 4, 5]
    startHour = 12
    endHour = 17

    if int(time.strftime("%w")) in dayArray:
        dtnow = datetime.datetime.now()
        dtStart = datetime.datetime(dtnow.year, dtnow.month, dtnow.day, startHour, 0, 0)
        dtEnd = datetime.datetime(dtnow.year, dtnow.month, dtnow.day, endHour, 0, 0)
        if dtnow.timestamp() > dtStart.timestamp() and dtnow.timestamp() < dtEnd.timestamp():
            return True
        else:
            return False
    else:
        return False


GPIO.setmode(GPIO.BOARD)
GPIO.setup(motorPin, GPIO.OUT)
sleep(1)

if __name__ == "__main__":
    flaskThread.start()
    while 1:

        if (checkTime() and runningMode == "auto") or runningMode == "ON":
            GPIO.output(motorPin, GPIO.LOW)
            #print("GPIO now LOW")

            if os.path.isfile("old.wav"):
                recordToFile("new.wav")
                combineFiles("old.wav","new.wav")

            else:
                recordToFile("old.wav")

            GPIO.output(motorPin, GPIO.HIGH)
            #print("GPIO now HIGH")

            playFile("old.wav")
            #print("")

        else:
            GPIO.output(motorPin, GPIO.HIGH)
            #print("not working now")
            sleep(1)
