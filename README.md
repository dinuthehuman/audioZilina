# Audio Script

## Dependencies

### Python

Python 3.4+

### Dependencies installieren - the big ones

```bash
sudo apt-get install python3 python3-pip git
```

### Libraries installieren

```bash
sudo apt-get install libasound-dev portaudio19-dev libportaudio2 libportaudiocpp0 libav-tools libffi5 libffi5-dev libffi5-dbg
sudo apt-get install python3-numpy python3-scipy python3-rpi.gpio
sudo pip3 install cffi sounddevice cffi pydub simpleaudio
```