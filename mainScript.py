#Bibliotheken
##############
import sounddevice as sd
from scipy.io.wavfile import write
import os.path
from pydub import AudioSegment
import simpleaudio as sa
import RPi.GPIO as GPIO
from time import sleep
import time
import datetime

#Konfiguration
###############
defaultSamplerate = 48000
duration = 5  #in Sekunden
motorPin = 21
channels = 1


def recordToFile(strName):
    print("Starting recording...")
    myrecording = sd.rec(int(defaultSamplerate*duration), samplerate=defaultSamplerate, channels=1)
    sd.wait()
    print("Timer reached, recording done")
    write(strName, defaultSamplerate, myrecording)
    sound1 = AudioSegment.from_file(strName)
    sound1.export(strName, format='wav')


def combineFiles(oldFile, newFile):
    print("Combining Files...")
    sound1 = AudioSegment.from_file(oldFile)
    sound2 = AudioSegment.from_file(newFile)
    combined = sound1.overlay(sound2)
    combined.export(oldFile, format='wav')
    print("...done")


def playFile(fileToPlay):
    print("Playback...")
    wave_obj = sa.WaveObject.from_wave_file(fileToPlay)
    play_obj = wave_obj.play()
    play_obj.wait_done()
    print("...done")


def checkTime():
    dayArray = [1, 2, 3, 4, 5]
    startHour = 12
    endHour = 17

    if int(time.strftime("%w")) in dayArray:
        dtnow = datetime.datetime.now()
        dtStart = datetime.datetime(dtnow.year, dtnow.month, dtnow.day, startHour, 0, 0)
        dtEnd = datetime.datetime(dtnow.year, dtnow.month, dtnow.day, endHour, 0, 0)
        if dtnow.timestamp() > dtStart.timestamp() and dtnow.timestamp() < dtEnd.timestamp():
            return True
        else:
            return False
    else:
        return False


GPIO.setmode(GPIO.BOARD)
GPIO.setup(motorPin, GPIO.OUT)
sleep(1)

while 1:

    if checkTime():
        GPIO.output(motorPin, GPIO.LOW)
        print("GPIO now LOW")

        if os.path.isfile("old.wav"):
            recordToFile("new.wav")
            combineFiles("old.wav","new.wav")

        else:
            recordToFile("old.wav")

        GPIO.output(motorPin, GPIO.HIGH)
        print("GPIO now HIGH")

        playFile("old.wav")
        print("")

    else:
        GPIO.output(motorPin, GPIO.HIGH)
        print("not working now")
        sleep(1)
